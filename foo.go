// Package foo provides foo'ish functionality
package foo


import (
    "math/rand"
)

var quotes = []string {
    "To foo or not to foo ... that is the question here!",
    "So long, and thanks for all the foo!",
    "Alea iacta foo!",
    "He who knows his foo and his bar, need not fear the outcome of a thousand battles!",
    "I came, I foo, I bar.",
    "Roses are red. Violets are Blue. I need a variable named. So I say 'foo'.",
}

func Quote() string {
    return quotes[rand.Intn(len(quotes))]
}

func Greet(name string) string {
    return "Hello, " + name + "!"
}

func GreetTwice(name string) string {
    return Greet(name) + "\n" + Greet(name)
}
